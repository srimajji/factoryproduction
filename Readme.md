#Factory Management

A sample app that gives you an optimal list of production cycle dates when given a list of date ranges

## Requirements
- [Node](https://nodejs.org/en/)

## Setup
- `git clone https://srimajji@bitbucket.org/srimajji/factoryproduction.git`
- `cd factoryproduction`
- `npm install`
- `npm start`

## Tests
- `npm test`

## API
- `GET: /`
- `GET: /productionCycles`

### GET: /productionCycles
```
curl -X GET \
  http://localhost:8080/productionCycles \
  -H 'Content-Type: application/json' \
  -d '[
    {
		"startingDay": "2020-01-03T00:00:00.000Z",
		"duration": 5
	},
	{
		"startingDay": "2020-01-09T00:00:00.000Z",
		"duration": 2
	},
	{
		"startingDay": "2020-01-24T00:00:00.000Z",
		"duration": 5
	},
	{
		"startingDay": "2020-01-16T00:00:00.000Z",
		"duration": 9
	},
	{
		"startingDay": "2020-01-11T00:00:00.000Z",
		"duration": 6
	}

]'
```
Response `200`
```
{
    "productionCycles": [
        {
            "startingDay": "2020-01-03T00:00:00.000Z",
            "duration": 5
        },
        {
            "startingDay": "2020-01-09T00:00:00.000Z",
            "duration": 2
        },
        {
            "startingDay": "2020-01-11T00:00:00.000Z",
            "duration": 6
        },
        {
            "startingDay": "2020-01-24T00:00:00.000Z",
            "duration": 5
        }
    ],
    "productionEndDate": "2020-01-28T08:00:00.000Z"
}
```

#### Error responses
Here are possible error messages you might receive depending on your request

Invalid duration count

`Status: 400`
```
{
    "message": "Duration has to be less than 1000 days",
    "value": {
        "startingDay": "2020-01-03T00:00:00.000Z",
        "duration": 5000
    }
}
```

Invalid month value

`Status: 400`
```
{
    "message": "Invalid date",
    "value": {
        "startingDay": "2020-13-03T00:00:00.000Z",
        "duration": 5
    }
}
```
Starting date less than current date

`Status: 400`
```
{
    "message": "Invalid date",
    "value": {
        "startingDay": "2019-01-03T00:00:00.000Z",
        "duration": 5
    }
}
```
Empty request

`Status: 400`
```
{
    "message": "Empty production cycles"
}
```
