import express from "express";

import IProductionCycle from "../models/IProductionCycle";
import { MAX_PRODUCTION_DURATION, MAX_PRODUCTION_SCHEDULES, MIN_PRODUCTION_DURATION } from "../utils/Constants";

export const calculateProductionCycles = (req: express.Request, res: express.Response) => {
	const dates: IProductionCycle[] = req.body;
	const minDuration = MIN_PRODUCTION_DURATION;
	const maxDuration = MAX_PRODUCTION_DURATION;
	const maxProductionCycles = MAX_PRODUCTION_SCHEDULES;

	// Validate request with the following conditions
	// List cannot be empty
	// Starting production date > current date
	// Production duration > 0 && < 10000
	// Total schedules < 100000

	if (!Object.keys(dates).length || !dates.length) {
		res.status(400).json({ message: "Empty production cycles" });
		return;
	} else if (dates.length >= maxProductionCycles) {
		res.status(413).json({ message: `Max threshold of ${maxProductionCycles} production cycles reached` });
		return;
	}

	const currentDate = new Date();

	// use a regular for loop for early break out when validation fails
	for (let i = 0; i < dates.length; i++) {
		const currentCycle = dates[i];
		const startDateTime = Date.parse(currentCycle.startingDay);
		const { duration } = currentCycle;
		if (duration <= minDuration) {
			res.status(400).json({
				message: `Duration has to be greater than ${minDuration} days`,
				value: currentCycle,
			});
			return;
		} else if (duration >= maxDuration) {
			res.status(400).json({
				message: `Duration has to be less than ${maxDuration} days`,
				value: currentCycle,
			});
			return;
		} else if (isNaN(startDateTime)) {
			res.status(400).json({ message: `Invalid date`, value: currentCycle });
			return;
		} else if (currentDate.getTime() >= startDateTime) {
			res.status(400).json({
				message: `Date must be greater than current date`,
				value: currentCycle,
			});
			return;
		}
	}

	// If valid request, run through and find non overlapping production cycle values
	let productionCycles: IProductionCycle[] = [];
	let productionEndDate = currentDate;
	dates
		.sort((a: IProductionCycle, b: IProductionCycle) => {
			// Since we are trying to find the most amount of non overlapping date submissions,
			// we start by sorting the list by production end date for each submission
			const firstEndDate = new Date(a.startingDay);
			firstEndDate.setHours(a.duration * 24);
			const secondEndDate = new Date(b.startingDay);
			secondEndDate.setHours(b.duration * 24);

			const firstEndDateTime = firstEndDate.getTime();
			const secondEndDateTime = secondEndDate.getTime();
			if (firstEndDateTime < secondEndDateTime) {
				return -1;
			}
			if (firstEndDateTime > secondEndDateTime) {
				return 1;
			}

			return 0;
		})
		.forEach((current: IProductionCycle) => {
			const currentStartDate = new Date(current.startingDay);
			const currentEndDate = new Date(current.startingDay);
			currentEndDate.setHours(current.duration * 24);

			if (currentStartDate.getTime() > productionEndDate.getTime()) {
				productionEndDate = currentEndDate;
				productionCycles.push(current);
			}
		});

	res.status(200).json({ productionCycles, productionEndDate });
};
