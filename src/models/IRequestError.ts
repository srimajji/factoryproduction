interface IRequestError {
	message: string;
	value?: any;
}

export default IRequestError;
