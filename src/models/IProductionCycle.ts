interface IProductionCycle {
	startingDay: string;
	duration: number;
}

export default IProductionCycle;
