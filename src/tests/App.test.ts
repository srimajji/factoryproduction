import * as request from "supertest";

import app from "../App";

describe("=====App=====", () => {
	test("test root route", async () => {
		const { status } = await request(app).get("/");
		expect(status).toBe(200);
	});
});
