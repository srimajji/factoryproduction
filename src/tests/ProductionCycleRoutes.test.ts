import * as request from "supertest";
import app from "../App";
import { MAX_PRODUCTION_SCHEDULES, MAX_PRODUCTION_DURATION, MIN_PRODUCTION_DURATION } from "../utils/Constants";

const getRandomInt = (max: number): number => {
	return Math.floor(Math.random() * Math.floor(max));
};

describe("======ProductionCycleRoutes=====", () => {
	describe("test various validation scenarios", () => {
		test("request body cannot be empty", async () => {
			const { status } = await request(app).get("/productionCycles");
			expect(status).toEqual(400);
		});

		test(`request cannot contain more than max possible schedules ${MAX_PRODUCTION_SCHEDULES}`, async () => {
			const requestBody = [];
			for (let i = 0; i < MAX_PRODUCTION_SCHEDULES + 1; i++) {
				const duration = getRandomInt(10);
				const startingDay = new Date();
				startingDay.setHours(getRandomInt(1000));
				requestBody.push({ startingDay, duration });
			}

			const { status } = await request(app)
				.get("/productionCycles")
				.send(requestBody);
			expect(status).toEqual(413);
		});

		test("productionCycle startingDate needs to be greater than currentDay", async () => {
			const duration = getRandomInt(10);
			const startingDay = new Date();
			startingDay.setHours(-getRandomInt(10));
			const requestBody = [
				{
					startingDay,
					duration,
				},
			];
			const { status } = await request(app)
				.get("/productionCycles")
				.send(requestBody);
			expect(status).toEqual(400);
		});

		test(`productionCycle duration cannot be greater than ${MAX_PRODUCTION_DURATION}`, async () => {
			const duration = MAX_PRODUCTION_DURATION + 1;
			const startingDay = new Date();
			startingDay.setHours(getRandomInt(10));
			const requestBody = [
				{
					startingDay,
					duration,
				},
			];
			const { status } = await request(app)
				.get("/productionCycles")
				.send(requestBody);
			expect(status).toEqual(400);
		});

		test(`productionCycle duration cannot be less than ${MIN_PRODUCTION_DURATION}`, async () => {
			const duration = MIN_PRODUCTION_DURATION - 1;
			const startingDay = new Date();
			startingDay.setHours(getRandomInt(10));
			const requestBody = [
				{
					startingDay,
					duration,
				},
			];
			const { status } = await request(app)
				.get("/productionCycles")
				.send(requestBody);
			expect(status).toEqual(400);
		});

		test("productionCycle startingDay needs to be valid day", async () => {
			const duration = getRandomInt(10);
			let startingDays = ["2020-01-02T00:00:76.0Z", "2020-01-32", "2020-14-03", "validString"];
			startingDays.forEach(async (startingDay: string) => {
				let requestBody = [
					{
						startingDay,
						duration,
					},
				];
				let { status } = await request(app)
					.get("/productionCycles")
					.send(requestBody);
				expect(status).toEqual(400);
			});

			let startingDay = "2020-1-2";
			let requestBody = [
				{
					startingDay,
					duration,
				},
			];
			let { status } = await request(app)
				.get("/productionCycles")
				.send(requestBody);
			expect(status).toEqual(200);
		});
	});

	describe("testing productionCycle scenario", () => {
		test("possible production cycle with a huge overlap over others should be discarded", async () => {
			const productionCycle1 = {
				startingDay: "2020-01-02T00:00:00.000Z",
				duration: 7,
			};
			const productionCycle2 = {
				startingDay: "2020-01-02T00:00:00.000Z",
				duration: 17,
			};
			const requestBody = [{ ...productionCycle1 }, { ...productionCycle2 }];
			const { status, body } = await request(app)
				.get("/productionCycles")
				.send(requestBody);

			const { productionCycles } = body;
			expect(status).toEqual(200);
			expect(productionCycles[0]).toEqual(productionCycle1);
		});

		test("possible production cycle submissions test1", async () => {
			const productionCycle1 = {
				startingDay: "2020-01-02T00:00:00.000Z",
				duration: 5,
			};
			const productionCycle2 = {
				startingDay: "2020-01-09T00:00:00.000Z",
				duration: 7,
			};
			const productionCycle3 = {
				startingDay: "2020-01-15T00:00:00.000Z",
				duration: 6,
			};
			const productionCycle4 = {
				startingDay: "2020-01-09T00:00:00.000Z",
				duration: 3,
			};
			const requestBody = [
				{ ...productionCycle1 },
				{ ...productionCycle2 },
				{ ...productionCycle3 },
				{ ...productionCycle4 },
			];
			const { status, body } = await request(app)
				.get("/productionCycles")
				.send(requestBody);

			const { productionCycles } = body;
			expect(status).toEqual(200);
			expect(productionCycles.length).toEqual(3);
			expect(productionCycles[0]).toEqual(productionCycle1);
		});
		test("possible production cycle submissions test1", async () => {
			const productionCycle1 = {
				startingDay: "2020-01-03T00:00:00.000Z",
				duration: 5,
			};
			const productionCycle2 = {
				startingDay: "2020-01-09T00:00:00.000Z",
				duration: 2,
			};
			const productionCycle3 = {
				startingDay: "2020-01-24T00:00:00.000Z",
				duration: 5,
			};
			const productionCycle4 = {
				startingDay: "2020-01-16T00:00:00.000Z",
				duration: 9,
			};
			const productionCycle5 = {
				startingDay: "2020-01-11T00:00:00.000Z",
				duration: 6,
			};
			const requestBody = [
				{ ...productionCycle1 },
				{ ...productionCycle2 },
				{ ...productionCycle3 },
				{ ...productionCycle4 },
				{ ...productionCycle5 },
			];
			const { status, body } = await request(app)
				.get("/productionCycles")
				.send(requestBody);

			const { productionCycles } = body;
			expect(status).toEqual(200);
			expect(productionCycles.length).toEqual(4);
			expect(productionCycles[0]).toEqual(productionCycle1);
		});
		test("possible production cycle submissions test2", async () => {
			const productionCycle1 = {
				startingDay: "2020-01-03T00:00:00.000Z",
				duration: 10,
			};
			const productionCycle2 = {
				startingDay: "2020-01-09T00:00:00.000Z",
				duration: 2,
			};
			const productionCycle3 = {
				startingDay: "2020-01-24T00:00:00.000Z",
				duration: 5,
			};
			const productionCycle4 = {
				startingDay: "2020-01-16T00:00:00.000Z",
				duration: 9,
			};
			const productionCycle5 = {
				startingDay: "2020-01-16T00:00:00.000Z",
				duration: 6,
			};
			const requestBody = [
				{ ...productionCycle1 },
				{ ...productionCycle2 },
				{ ...productionCycle3 },
				{ ...productionCycle4 },
				{ ...productionCycle5 },
			];
			const { status, body } = await request(app)
				.get("/productionCycles")
				.send(requestBody);

			const { productionCycles } = body;
			expect(status).toEqual(200);
			expect(productionCycles.length).toEqual(3);
			expect(productionCycles[0]).toEqual(productionCycle2);
		});
		test("should return one possible production cycle if all ranges are the same", async () => {
			const productionCycle1 = {
				startingDay: "2020-01-03T00:00:00.000Z",
				duration: 5,
			};
			const requestBody = [{ ...productionCycle1 }, { ...productionCycle1 }, { ...productionCycle1 }];
			const { status, body } = await request(app)
				.get("/productionCycles")
				.send(requestBody);

			const { productionCycles } = body;
			expect(status).toEqual(200);
			expect(productionCycles.length).toEqual(1);
			expect(productionCycles[0]).toEqual(productionCycle1);
		});
	});
});
