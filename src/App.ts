import * as express from "express";
import * as morgan from "morgan";
import * as helmet from "helmet";
import * as bodyParser from "body-parser";

import { stream } from "./utils/Logger";
import { calculateProductionCycles } from "./routes";

class App {
	public app: express.Application;
	constructor() {
		this.app = express();

		this.config();
		this.configRoutes();
	}

	private config() {
		this.app.set("env", process.env.NODE_ENV);
		this.app.set("port", Number.parseInt(process.env.NODE_PORT) || 8080);
		this.app.use(morgan("combined", { stream }));
		this.app.use(helmet());
		this.app.use(bodyParser.json());
		this.app.use(bodyParser.urlencoded({ extended: true }));
	}

	private configRoutes() {
		this.app.get("/", (req: express.Request, res: express.Response) => {
			res.json({ message: "Factory management app" });
		});

		this.app.get("/productionCycles", calculateProductionCycles);
	}
}

export default new App().app;
